﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project1
{


   public class Program
    { 
        static void Main(string[] args)
        {
            Console.WriteLine("Title: P1 - Room Size Calculator Using Classes");
            Console.WriteLine("Author: Casey B. Hargarther");
            DateTime rightNow = DateTime.Now;
            string format = "ddd M/d/yy h:mm:ss t";
            Console.Write("Now: ");
            Console.WriteLine(rightNow.ToString(format));

            string rtype = "";
            double rlength = 10.0;
            double rwidth = 10.0;
            double rheight = 10.0;

            Console.WriteLine("\nNote: Expected values in feet.");

            // Calling out Default Values
            Dimensions dimension1 = new Dimensions();

            // Get Default Room Type
            Console.Write("Room Type: ");
            Console.WriteLine(dimension1.GetType());

            // Get Default Length
            Console.Write("Room Length: ");
            Console.WriteLine(dimension1.GetLength());
            
            // Get Default Width
            Console.Write("Room Width: ");
            Console.WriteLine(dimension1.GetWidth());
            
            // Get Default Height
            Console.Write("Room Height: ");
            Console.WriteLine(dimension1.GetHeight());

            // Get Default Area
            Console.Write("Room Area: ");
            Console.WriteLine(dimension1.GetArea().ToString("F2") + " sq ft");

            // Get Default Volume
            Console.Write("Room Volume: ");
            Console.WriteLine(dimension1.GetVolume().ToString("F2") + " cu ft");

            // Get Default Volume
            Console.Write("Room Volume: ");
            Console.WriteLine((dimension1.GetVolume() / 27).ToString("F2") + " cu yd");

            // Console.WriteLine("Room Type: Default");

            Console.WriteLine("\nModify " + dimension1.GetType() +" room object's data member values:");
            Console.WriteLine("Using setter/getter methods:");

            // Modifying Room Type
            Console.Write("Room Type: ");
            rtype = Console.ReadLine();

            // Modifying Room Length
            Console.Write("Room Length: ");
            while (!double.TryParse(Console.ReadLine(), out rlength))
            {
                Console.WriteLine("Room length must be numeric");
            }


            //  dimension1.SetLength(length);


            // Modifying Room Width
            Console.Write("Room Width: ");
            while (!double.TryParse(Console.ReadLine(), out rwidth))
            {
                Console.WriteLine("Room width must be numeric");
            }
         
            // Modifying Room Height
            Console.Write("Room Height: ");
            while (!double.TryParse(Console.ReadLine(), out rheight))
            {
                Console.WriteLine("Room height must be numeric");
            }

           Console.WriteLine("\nDisplay " + dimension1.GetType() + " room object's new data member values:");
            // **** use setter/mutator methods ****
            // Modify Dimensions 

            dimension1.SetType(rtype);
            dimension1.SetLength(rlength);
            dimension1.SetWidth(rwidth);
            dimension1.SetHeight(rheight);

            // Get Modified Room Type
            Console.Write("Room Type: ");
            Console.WriteLine(dimension1.GetType());

            // Get Modified Length
            Console.Write("Room Length: ");
            Console.WriteLine(dimension1.GetLength());

            // Get Modified Width
            Console.Write("Room Width: ");
            Console.WriteLine(dimension1.GetWidth());

            // Get Modified Height
            Console.Write("Room Height: ");
            Console.WriteLine(dimension1.GetHeight());

            // Get Modified Area
            Console.Write("Room Area: ");
            Console.WriteLine(dimension1.GetArea().ToString("F2") + " sq ft");
            
            // Get Modified Volume
            Console.Write("Room Volume: ");
            Console.WriteLine(dimension1.GetVolume().ToString("F2") + " cu ft");

            // Get Modified Room Volume with division of 27
            Console.Write("Room Volume: ");
            Console.WriteLine((dimension1.GetVolume() /27).ToString("F2") + " cu yd");

            // Calls the Parameterized constructor
            Console.WriteLine("\nCall parameterized constructor (accepts four arguments):");

            // Modifying Room Type
            Console.Write("Room Type: ");
            rtype = Console.ReadLine();

            // Modifying Room Length
            Console.Write("Room Length: ");
            while (!double.TryParse(Console.ReadLine(), out rlength))
            {
                Console.WriteLine("Room length must be numeric");
            }

            // Modifying Room Width
            Console.Write("Room Width: ");
            while (!double.TryParse(Console.ReadLine(), out rwidth))
            {
                Console.WriteLine("Room width must be numeric");
            }

            // Modifying Room Height
            Console.Write("Room Height: ");
            while (!double.TryParse(Console.ReadLine(), out rheight))
            {
                Console.WriteLine("Room height must be numeric");
            }

            // Calls new dimension from the class
            Dimensions dimension2 = new Dimensions(rwidth, rlength, rheight, rtype);

            Console.Write("Room Type: ");
            Console.WriteLine(dimension2.GetType());

            // Get New Modified Length
            Console.Write("Room Length: ");
            Console.WriteLine(dimension2.GetLength());

            // Get New Modified Width
            Console.Write("Room Width: ");
            Console.WriteLine(dimension2.GetWidth());

            // Get New Modified Height
            Console.Write("Room Height: ");
            Console.WriteLine(dimension2.GetHeight());

            // Get New Modified Area
            Console.Write("Room Area: ");
            Console.WriteLine(dimension2.GetArea().ToString("F2") + " sq ft");

            // Get New Modified Volume
            Console.Write("Room Volume: ");
            Console.WriteLine(dimension2.GetVolume().ToString("F2") + " cu ft");

            // Get Modified Room Volume with division of 27
            Console.Write("Room Volume: ");
            Console.WriteLine((dimension2.GetVolume() / 27).ToString("F2") + " cu yd");

            Console.WriteLine("\nPress Enter to exit...");
           Console.Read();
        }
    }
}
