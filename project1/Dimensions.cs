﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project1
{
    public class Dimensions
    {

        private string type;
        private double length;
        private double width;
        private double height;

        // Default Values
        public Dimensions()
        {

            // Sets Default variables
            type = "Default";
            length = 10.0;
            width = 10.0;
            height = 10.0;

            Console.WriteLine("\nCreating " + this.type + " room object from default constructor (accepts no arguments):");
        }


        public Dimensions(double w = 0.0, double l = 0.0, double h = 0.0, string t = "")
        {
            type = t;
            length = l;
            width = w;
            height = h;
            
            Console.WriteLine("\nCreating " + this.type + " room object from parameterized constructor (accepts four arguments):");
        }

        // setter method
        public void SetWidth(double w=0.0)
        {
            // Sets the Width
            width = w;
        }

        public void SetType(string t="default")
        {
            // Sets the string type
            type = t;
        }

        public void SetLength(double l=0.0)
        {
            // Sets the Length
            length = l;
        }

        public void SetHeight(double h=0.0)
        {
            // Sets the Height
            height = h;
        }

        // *** accessor methods ***
        // getter method
        
        public string GetType()
        {
            // Returns the Type
            return type;
        }

        public double GetWidth()
        {
            // Returns the width
            return width;
        }

        public double GetLength()
        {
            // Returns the Length
            return length;
        }

        public double GetHeight()
        {
            // Returns the Height
            return height;
        }

        public double GetArea()
        {
            // Returns the Area
            // Uses Formula A = LW
            return length * width;
        }

        public double GetVolume()
        {
            // Returns the Volume
            // Uses Formula V = LWH
            return length * width * height;
        }
    }
}